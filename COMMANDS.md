# The top of the iceberg

## Stand alone app

Compile.

```
cd ./backend
GOOS=linux GOARCH=amd64 go build -tags netgo -o app
```

Run it.

```
# frontend
cd ./frontend
# If it's first time running the python app, create the virtual env and install pakcages:
virtualenv venv
pip install -r requirements.txt
# activate virtual env
source venv/bin/activate
# run the app (set the port you want to hit)
PORT=8081 python app.py
```

```
# backend
cd ./backend
./app
```

We can specify the backend name passing it as data to the http request.

```
# API version
curl -X POST localhost:8081/api/getwinner -H "Content-Type: application/json"  -d '{"url":"localhost:8080"}'
# HTML version
curl -X POST localhost:8081/getwinner -H "Content-Type: application/json"  -d '{"url":"localhost:8080"}'
```

Once it's done, deactivate the virtual env:

```
cd ./frontend
deactivate
```

## Docker

Two apps and a network

Build

```
docker build -t jsltalks001-backend .
docker build -t jsltalks001-frontend .
```

_Note: both images were uploaded to docker.io, so you can download them from that site. We will be using them._

Run

```
docker network create JSL
docker run -it --rm --network JSL --name backend docker.io/juanmatias/jsltalks001-backend .
docker run -it --rm -p 8080:80 --network JSL --name frontend docker.io/juanmatias/jsltalks001-frontend
```

Query the endpoint, note we changed the backend name... why?:

```
curl localhost:8080/
curl -X POST localhost:8080/api/getwinner -H "Content-Type: application/json"  -d '{"url":"backend:8080"}'
curl -X POST localhost:8080/getwinner -H "Content-Type: application/json"  -d '{"url":"backend:8080"}'
```

If you check the containers running with `docker ps`, you will se the _frontend_ and the _backend_. If you instapect the _JSL_ network runnint `docker network inspect JSL | jq '.[].Containers'` you will see the two containers are added into the network.

Then, a sort of DNS in the network keep the addresses. Try pinging from one container to the other one:

```
docker exec $(docker ps | grep frontend | awk '{print $1}') ping backend -c 5
docker exec $(docker ps | grep backend | awk '{print $1}') ping frontend -c 5
```

## K3s

Start and config kubectl access:

```
sudo systemctl start k3s.service
sudo k3s kubectl config view --raw > /tmp/kubeconfig.file
export KUBECONFIG=/tmp/kubeconfig.file
```

Test:

```
kubectl get nodes
```

### POD

Deploy

```
cd k8s/pods
kubectl create -f pod-backend.yaml
kubectl create -f pod-frontend.yaml
```

Check

```
kubectl get pod
```

You should see the following output (or similar):

```
NAME       READY   STATUS    RESTARTS   AGE
backend    1/1     Running   0          18m
frontend   1/1     Running   0          8m3s
```
Do the port-forward and test it

```
kubectl port-forward frontend 6060:80
curl locahost:6060
```

These commands must rise an error:

```
curl localhost:6060/api/getwinner
curl -X POST localhost:6060/api/getwinner -H "Content-Type: application/json"  -d '{"url":"backend:8080"}'
```

Se let's specify the backend's IP and port. First get the IP:

```
kubectl exec backend -it -- ip addr
curl -X POST localhost:6060/api/getwinner -H "Content-Type: application/json"  -d '{"url":"<insert here the ip address>:8080"}'
```

We need to add the ip address, and it can change... so we need to improve it. Wait a little.

So far, it works!

Let's add two useful commands:

```
kubectl describe pod frontend
kubectl logs frontend
kubectl logs -f frontend
```

Delete the pods and return to presentation:

```
kubectl delete -f pod-backend.yaml
kubectl delete -f pod-frontend.yaml
```

### Services, part 1

Deploy

```
cd k8s/service001
kubectl create -f pod-backend.yaml
kubectl create -f pod-frontend.yaml
```

Check

```
kubectl get pod
```

You should see the following output (or similar):

```
NAME       READY   STATUS    RESTARTS   AGE
backend    1/1     Running   0          18m
frontend   1/1     Running   0          8m3s
```

Now, deploy the services:

```
kubectl create -f service-backend.yaml
kubectl create -f service-frontend.yaml
```

Check them:

```
kubectl get svc
```

You should see something like this:

```
NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
backend      ClusterIP   10.43.190.98    <none>        8080/TCP   11s
frontend     ClusterIP   10.43.255.166   <none>        8080/TCP   10s
```
_IPs will be different_

This time, we will port-forward to the service's IP (actually, it's a trick, it will forward to a random pod behind the service):

```
kubectl port-forward service/frontend 6060:8080
```

Try it:

```
curl localhost:6060
```

We get:

```
{"hostname": "frontend"}
```

So far, so good. Let's try to get a winner:

```
curl localhost:6060/api/getwinner
curl -X POST localhost:6060/api/getwinner -H "Content-Type: application/json"  -d '{"url":"backend:8080"}'
```

While the first one fails, the second one succeed with the backend pointing to _backend:8080_.

We solved the IP and persistent name issue.

Delete the pods and services:

```
kubectl delete -f pod-backend.yaml -f pod-frontend.yaml -f service-backend.yaml -f service-frontend.yaml
```

### Deployments

Deploy them:

```
cd k8s/deployments
kubectl apply -f deployment-backend.yaml -f deployment-frontend.yaml -f service-backend.yaml -f service-frontend.yaml
```

Check them:

```
kuebctl get pod
```

Oops, what happened to the names?

```
NAME                        READY   STATUS    RESTARTS   AGE
frontend-589fc8d958-vddbk   1/1     Running   0          22m
backend-5874df976b-lkc92    1/1     Running   0          22m
```

Since, these pods belongs to a Deployment, and a Deployment can have any number of Pods, they are named with the Deployment's name and a hash.

We can check the services and the deployments as well.

```
kubectl get svc
kubectl get deployments
```

Again we will try our app:

```
kubectl port-forward service/frontend 6060:8080
curl localhost:6060
curl -X POST localhost:6060/api/getwinner -H "Content-Type: application/json"  -d '{"url":"backend:8080"}'
```

It's working. Again, how Kubernetes knows what pods hit from each service. Selectors and labels. Run these commands:

```
# Get the pod's name
kubectl get po
kubectl get po <podname> -o yaml
kubectl get service/frontend -o yaml
```

They show the yaml manifest for pod and service. Check the labels from the former matches the selector of the former.

What if we have a big demand for the app and we need more capacity on the backend side?

Deployment's magic. We can have any number of pods for backend deployment!

Let's scale up the number.

```
kubectl scale deployment/backend --replicas=3
```

Here, we're saying: set the number of desired replicas to 3 for deployment named backend.

And Kubernetes will try to reach the desired state.

Oh, now we have three pods for backend!

```
╰─ kgp
NAME                        READY   STATUS    RESTARTS   AGE
frontend-589fc8d958-vddbk   1/1     Running   0          33m
backend-5874df976b-lkc92    1/1     Running   0          33m
backend-5874df976b-qv6fs    1/1     Running   0          4s
backend-5874df976b-f6t92    1/1     Running   0          4s
```

Note the name of backend's pods.

Try again the port-forward and the curl command. It should work. Backend service will redirect traffic in a roundrobin fashion to each pod.

Finally, you can try to do the same with the frontend:

```
kubectl scale deployment/frontend --replicas=2
```

_this time we set replica=2_

And portforward , etc.

## More on services

So far, we used the default service type: ClusterIP.

Delete the frontend service:

```
kubectl delete -f service-frontend.yaml
```

Under _spec_ add: `type: LoadBalancer` and re deploy the service:

```
kubectl apply -f service-frontend.yaml
```

Now get the services again:

```
╰─ kubectl get svc
NAME         TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)          AGE
backend      ClusterIP      10.43.211.104   <none>           8080/TCP         54m
frontend     LoadBalancer   10.43.117.224   192.168.69.154   8080:32570/TCP   36s
```

This time, the frontend service has an external IP... and, wait, it's the same we have on the host!

Yes, since we're using K3s, the cluster is running in our PC, then the external load balancer will use this IP.

Let's try it! This time we won't need port-forward, just:

```
curl -X POST localhost:8080/api/getwinner -H "Content-Type: application/json"  -d '{"url":"backend:8080"}'
```

Delete deployments: 

```
kubectl delete -f pod-backend.yaml -f pod-frontend.yaml -f service-backend.yaml -f service-frontend.yaml
```

## Ingress

Deploy

```
cd k8s/ingress
kubectl apply -f deployment-backend.yaml -f deployment-frontend.yaml -f service-backend.yaml -f service-frontend.yaml -f ingress-frontend.yaml
```

Check

```
kubectl get all
kubectl get ing
```

The first command will get pods, services, deplouments and replicasets.
The second one will get the ingress:

```
╰─ kubectl get ing
NAME               HOSTS   ADDRESS          PORTS   AGE
frontend-ingress   *       192.168.69.154   80      2m7s
```

Again, our host's IP! It's listening on this IP, port 80, will get any host.

Let's try it:

```
╰─ curl localhost     

{"hostname": "frontend-589fc8d958-n7mrt"}
```

Root path is working. Let's get a winner:

```
╰─ curl -X POST localhost/api/getwinner -H "Content-Type: application/json"  -d '{"url":"backend:8080"}'   

{"the-winner-is": "448"}
```

## Notes

### Visualize

To visualize K8s objects we're using [KubeView](https://github.com/benc-uk/kubeview).

Download the helm chart (in the repo) and install it. Then port-forward to the service, e.g.:

```
kubectl port-forward -n kubeview service/kubeview 4040:80
```
